#include <windows.h>
#include <fstream>
#include <utility>
#include <vector>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <string>

#include "MainCaller.h"
#include "caffe/caffe.hpp"
#include "caffe/util/io.hpp"
#include "caffe/proto/caffe.pb.h"

#include <glog/logging.h>
#include <leveldb/db.h>
#include <stdint.h>
#include <leveldb/write_batch.h>
#include "FossilRecognizerUtil.h"
#include <cuda_runtime.h>

#include <opencv/highgui.h>
#include <opencv/cv.h>

using namespace caffe;
using namespace std;
using namespace cv;

#define COOKIE_CUT_SIZE	32

BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	return TRUE;
}
__declspec(dllexport) int __stdcall Training_CNN(char* SolverProtoFile, char* ResumePointFile)
{
	SolverParameter solver_param;
	ReadProtoFromTextFileOrDie(SolverProtoFile, &solver_param);

	SGDSolver<float> solver(solver_param);

	//if (ResumePointFile != NULL) {
	//	solver.Solve(ResumePointFile);
	//}
	//else {
		solver.Solve();
	//}

	return 0;
}
__declspec(dllexport) int __stdcall Resizing_Fossil_Image(char* InputFolder, char* ListFile)
{
	std::ifstream infile(ListFile);
	std::vector<std::pair<string, int> > lines;
	string filename;
	string root_folder(InputFolder);
	int label;

	while (infile >> filename >> label)
	{
		lines.push_back(std::make_pair(filename, label));
	}

	for (int line_id = 0; line_id < lines.size(); ++line_id)
	{
		Mat CookieCut_Org = imread(root_folder + lines[line_id].first);
		Mat CookieCut_New;

		resize(CookieCut_Org, CookieCut_New, Size(COOKIE_CUT_SIZE, COOKIE_CUT_SIZE));
		imwrite(root_folder + lines[line_id].first, CookieCut_New);
	}

	return 1;
}
__declspec(dllexport) int __stdcall Convert_Fossil_Image(char* InputFolder, char* ListFile, char* DBName, int Shuffle)
{
	std::ifstream infile(ListFile);
	std::vector<std::pair<string, int> > lines;
	string filename;

	int label;

	while (infile >> filename >> label)
	{
		lines.push_back(std::make_pair(filename, label));
	}
	if (Shuffle == '1')
	{
		// randomly shuffle data
		std::random_shuffle(lines.begin(), lines.end());
	}

	leveldb::DB* db;
	leveldb::Options options;
	options.error_if_exists = true;
	options.create_if_missing = true;
	options.write_buffer_size = 268435456;

	leveldb::Status status = leveldb::DB::Open(options, DBName, &db);

	string root_folder(InputFolder);
	Datum datum;
	int count = 0;
	const int kMaxKeyLength = 256;
	char key_cstr[kMaxKeyLength];
	leveldb::WriteBatch* batch = new leveldb::WriteBatch();
	int data_size;
	bool data_size_initialized = false;

	for (int line_id = 0; line_id < lines.size(); ++line_id)
	{
		if (!ReadImageToDatum(root_folder + lines[line_id].first, lines[line_id].second, &datum))
		{
			continue;
		}
		if (!data_size_initialized)
		{
			data_size = datum.channels() * datum.height() * datum.width();
			data_size_initialized = true;
		}
		else
		{
			const string& data = datum.data();
		}
		// sequential
		snprintf(key_cstr, kMaxKeyLength, "%08d_%s", line_id, lines[line_id].first.c_str());
		string value;
		// get the value
		datum.SerializeToString(&value);
		batch->Put(string(key_cstr), value);

		if (++count % 1000 == 0)
		{
			db->Write(leveldb::WriteOptions(), batch);

			delete batch;
			batch = new leveldb::WriteBatch();
		}
	}
	// write the last batch
	if (count % 1000 != 0)
	{
		db->Write(leveldb::WriteOptions(), batch);
	}

	delete batch;
	delete db;

	return 1;
}
__declspec(dllexport) int __stdcall Fossil_Recognizer(ePROCESS_TYPE type, char* Prototxt, char* Caffemodel, int NumOfLable, int InputDataSize, BYTE* CurImage, int* Top5_Idx, float* Top5_Score)
{
	Mat InputImage = cv::Mat(InputDataSize, InputDataSize, CV_8UC3, CurImage);
	int device_id = 0;
	int top5_idx[5];
	float *output = new float[NumOfLable];
	vector<Blob<float>*> input_vec;

	Caffe::set_phase(Caffe::TEST);

	if(type == PROCESS_CPU)
	{
		Caffe::set_mode(Caffe::CPU);
	}
	else if(type == PROCESS_GPU)
	{
		Caffe::set_mode(Caffe::GPU);
		Caffe::SetDevice(device_id);
	}
	
	Net<float> caffe_test_net(Prototxt);
	caffe_test_net.CopyTrainedLayersFrom(Caffemodel);
	
	Blob<float> blob(1, 3, InputDataSize, InputDataSize);

	caffe::Datum datum;
	float mean_val[3] = {0.0, 0.0, 0.0}; 


	for (int k = 0; k < 3; k++)
	{
		float SumValue = 0.0f;

		for (int j = 0; j < InputDataSize; j++)
		{
			for (int i = 0; i < InputDataSize; i++)
			{
				SumValue += (float)((unsigned char)InputImage.at<cv::Vec3b>(i, j)[k]);
			}
		}
		mean_val[k] = SumValue / (float)(InputDataSize*InputDataSize);
	}

	for (int j = 0; j < InputDataSize; j++)
	{
		for (int i = 0; i < InputDataSize; i++)
		{
			blob.mutable_cpu_data()[blob.offset(0, 0, i, j)] = (float)((unsigned char)InputImage.at<cv::Vec3b>(i, j)[0] - mean_val[0]);
			blob.mutable_cpu_data()[blob.offset(0, 1, i, j)] = (float)((unsigned char)InputImage.at<cv::Vec3b>(i, j)[1] - mean_val[1]);
			blob.mutable_cpu_data()[blob.offset(0, 2, i, j)] = (float)((unsigned char)InputImage.at<cv::Vec3b>(i, j)[2] - mean_val[2]);
		}
	}

	input_vec.push_back(&blob);
	
	float loss;
	const vector<Blob<float>*>& result = caffe_test_net.Forward(input_vec, &loss);

	for(int i = 0; i < NumOfLable; i++)
	{
		output[i] = result[0]->cpu_data()[i];
	}

	get_top5(NumOfLable, output, top5_idx);	
	input_vec.clear();

	for(int i = 0; i < 5; i++)
	{
		Top5_Idx[i] = top5_idx[i];
		Top5_Score[i] = output[i];
	}
	
	return top5_idx[0];		// 가장 확률이 높은 top1 index 리턴
}