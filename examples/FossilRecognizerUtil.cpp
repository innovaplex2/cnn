#include "FossilRecognizerUtil.h"

void bubble_sort(int NumOfData, float *feature, int *sorted_idx)
{
	float tmp;
	int tmp_idx;

	for(int i = 0; i < NumOfData; i++)
	{
		sorted_idx[i] = i;
	}

	for(int i = 0; i < NumOfData; i++)
	{
		for(int j = 0; j < NumOfData-1; j++)
		{
			if(feature[j] < feature[j + 1])
			{
				tmp = feature[j];
				feature[j] = feature[j + 1];
				feature[j + 1] = tmp;

				tmp_idx = sorted_idx[j];
				sorted_idx[j] = sorted_idx[j + 1];
				sorted_idx[j + 1] = tmp_idx;
			}
		}
	}
}

void get_top5(int NumOfData, float *feature, int arr[5])
{
	int *sorted_idx = new int[NumOfData];
	memset(sorted_idx, 0, sizeof(int)*NumOfData);

	bubble_sort(NumOfData, feature, sorted_idx);

	for(int i = 0; i<5; i++)
	{
		arr[i] = sorted_idx[i];
	}
}

void get_label(char filename[256], char label[][512], int NumOfLable)
{
	FILE *fp = fopen(filename, "r");

	for(int i = 0; i < 10; i++)
	{
		fgets(label[i], 512, fp);

		for(int j = 0; j < 512; j++)
		{
			if (label[i][j] == '\n' || label[i][j] == ',')
			{
				label[i][j] = '\0';
				break;
			}
		}
	}

	fclose(fp);
}

void draw_output(IplImage *img, float *output, int *idx, char label[][512])
{
	int i = 0;
	CvFont font;
	char str[256];
	cvInitFont(&font, CV_FONT_HERSHEY_PLAIN, 1.5, 1.5, 0, 2, 8);

	for (i = 0; i<5; i++)
	{
		sprintf(str, "%s, %.03f", label[idx[i]], output[i]);
		if (i == 0)
			cvPutText(img, str, cvPoint(10, 30 + i * 30), &font, CV_RGB(255, 0, 0));
		else if (i == 1)
			cvPutText(img, str, cvPoint(10, 30 + i * 30), &font, CV_RGB(0, 0, 255));
		else
			cvPutText(img, str, cvPoint(10, 30 + i * 30), &font, CV_RGB(255, 255, 0));
	}
}