#pragma once
#include <opencv/highgui.h>
#include <opencv/cv.h>

typedef enum  {
	PROCESS_CPU,
	PROCESS_GPU
}ePROCESS_TYPE;

using namespace cv;

__declspec(dllexport) int __stdcall Training_CNN(char* SolverProtoFile, char* ResumePointFile);
__declspec(dllexport) int __stdcall Fossil_Recognizer(ePROCESS_TYPE type, char* Prototxt, char* Caffemodel, int NumOfLable, int InputDataSize, BYTE* CurImage, int* Top5_Idx, float* Top5_Score);