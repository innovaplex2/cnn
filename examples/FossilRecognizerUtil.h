#include <stdio.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <string>
#include <stdint.h>

#include <cuda_runtime.h>

#include <opencv/highgui.h>
#include <opencv/cv.h>

#include <leveldb/db.h>
#include <google/protobuf/text_format.h>
#include "caffe/proto/caffe.pb.h"

using namespace std;

void bubble_sort(int NumOfData, float *feature, int *sorted_idx);
void get_top5(int NumOfData, float *feature, int arr[5]);
void get_label(char filename[256], char label[][512], int NumOfLable);
void draw_output(IplImage *img, float *output, int *idx, char label[][512]);